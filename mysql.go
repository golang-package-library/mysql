package mysql

import (
	"database/sql"
	"fmt"

	env "gitlab.com/golang-package-library/env"
	logger "gitlab.com/golang-package-library/logger"
)

// Database modal
type Databases struct {
	DB *sql.DB
}

// NewDatabase creates a new database instance
func NewDatabases(env env.Env, zapLogger logger.Logger) Databases {

	username := env.DBUsername
	password := env.DBPassword
	host := env.DBHost
	port := env.DBPort
	dbname := env.DBName

	url := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", username, password, host, port, dbname)

	db, err := sql.Open("mysql", url)

	if err != nil {
		zapLogger.Zap.Info("Url: ", url)
		zapLogger.Zap.Info("Mysql Connection Refused")
		zapLogger.Zap.Panic(err)
	}

	zapLogger.Zap.Info("Database connection established")

	return Databases{
		DB: db,
	}
}
